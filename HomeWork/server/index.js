const express = require('express');
// import express from 'express'

const app = express();

const port = 8085;

const cart = [{"title": "hello"}]
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'); // to enable calls from every domain 
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE'); // allowed actiosn
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization'); 

  if (req.method === 'OPTIONS') {
    return res.sendStatus(200); // to deal with chrome sending an extra options request
  }

  next(); // call next middlewer in line
});

app.get('/api/cart', (req, res) => {
  res.send(cart)
})


app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
