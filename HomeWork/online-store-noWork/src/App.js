import './App.css';
import React from 'react';
import axios from 'axios';
import Navbar from './components/Navbar/Navbar';
import Header from './components/Header/Header';
import Slider from './components/Slider/Slider';
import Welcome from './components/Welcome/Welcome';
import LatestMusic from './components/LatestMusic/LatestMusic';
import AlbomSale from './components/AlbomSale/AlbomSale';
import Publishers from './components/Publishers/Publishers';
import Footer from './components/Footer/Footer';
import Category from './components/Category/Category';
import Fragment from './components/Fragment/Fragment';
import headphone from './components/Category/images/01.svg';
import disk from './components/Category/images/02.svg';
import calendar from './components/Category/images/03.svg';
import BtnAddToCart from './components/BtnAddToCart/BtnAddToCart';
import Star from './components/Star/Star'
import TitlePublisher from './components/TitlePublisher/TitlePublisher';



class App extends React.Component {
  // state = {
  //   // category: [
  //   //   {
  //   //     title: "check our cd collection",
  //   //     content: "Donec pede justo, fringilla vel, al, vulputate eget.",
  //   //     backgroundColor: "category",
  //   //     icon: headphone,
  //   //   },
  //   //   {
  //   //     title: "listen before purchase",
  //   //     content: "Donec pede justo, fringilla vel, al, vulputate eget.",
  //   //     backgroundColor: "category category-color-red",
  //   //     icon: disk,
  //   //   },
  //   //   {
  //   //     title: "upcoming events",
  //   //     content: "Donec pede justo, fringilla vel, al, vulputate eget.",
  //   //     backgroundColor: "category",
  //   //     icon: calendar,
  //   //   }
  //   // ],

  //   products: [
  //     {
  //       title: "Oh my Deer",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       price: "14.99",
  //     },
  //     {
  //       title: "Oh my Deer",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       price: "17.99",
  //     },
  //     {
  //       title: "Oh my Deer",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       price: "18.99",
  //     },
  //     {
  //       title: "Oh my Deer",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       price: "22.99",
  //     },
  //   ],

  //   titleBlock: [
  //     {
  //       title: "Latest arrivals in musica",
  //     },
  //     {
  //       title: "Albums currently on sale"
  //     },
  //     {
  //       title: "Our most important publishers"
  //     },
  //   ],

  //   saleProducts: [
  //     {
  //       title: "Caribou",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       closePrice: "22.99",
  //       openPrice: "18.99"
  //     },
  //     {
  //       title: "Aqualung",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       closePrice: "12.99",
  //       openPrice: "1.99"
  //     },
  //     {
  //       title: "Strange to years",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       closePrice: "82.99",
  //       openPrice: "16.99"
  //     },
  //     {
  //       title: "Ravenna",
  //       author: "by Artist",
  //       star: "",
  //       content: "lorem ipsum? jetr sfd se44tr hello world",
  //       closePrice: "62.99",
  //       openPrice: "48.99"
  //     },
  //   ]

  // }

  constructor(props) {
    super(props);
    this.state = {
      category: [],
      products: [],
      titleBlock: [],
      saleProducts: []
    };
  };

  addStateJson = () => {
    axios.get("http://localhost:3000/db.json")
      .then((response) => {
        const categoryJSON = response.data.category;
        const productsJSON = response.data.products;
        const titleBlockJSON = response.data.titleBlock;
        const saleProductsJSON = response.data.saleProducts;
        this.setState({ category: categoryJSON });
        this.setState({ products: productsJSON });
        this.setState({ titleBlock: titleBlockJSON });
        this.setState({ saleProducts: saleProductsJSON });
      })
      console.log(this.state.category);
  }


  render() {
    const { category, products, titleBlock, saleProducts } = this.state;

    return (
      <div className="App">
        <button onClick={this.addStateJson}></button>
        <Navbar />
        <Header />
        <Slider />
        <Welcome />
        <Fragment>
          <div>
            <Category iconsCategory={category[0].icon} categoryTitle={category[0].title} categoryContent={category[0].content} background={category[0].backgroundColor} /><span>  </span>
            <Category iconsCategory={category[1].icon} categoryTitle={category[1].title} categoryContent={category[1].content} background={category[1].backgroundColor} /><span>  </span>
            <Category iconsCategory={category[2].icon} categoryTitle={category[2].title} categoryContent={category[2].content} background={category[2].backgroundColor} />
          </div>
        </Fragment>
        <TitlePublisher titlePublisher={titleBlock[0].title} />
        <Fragment>
          <div className="container">
            <LatestMusic latestTitle={products[0].title} latestAuthor={products[0].author} latestContent={products[0].content} latestStar={<Star />} latestPrice={products[0].price} latestBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <LatestMusic latestTitle={products[1].title} latestAuthor={products[1].author} latestContent={products[1].content} latestStar={<Star />} latestPrice={products[1].price} latestBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <LatestMusic latestTitle={products[2].title} latestAuthor={products[2].author} latestContent={products[2].content} latestStar={<Star />} latestPrice={products[2].price} latestBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <LatestMusic latestTitle={products[3].title} latestAuthor={products[3].author} latestContent={products[3].content} latestStar={<Star />} latestPrice={products[3].price} latestBtnCart={<BtnAddToCart />} />
          </div>
        </Fragment>
        <TitlePublisher titlePublisher={titleBlock[1].title} />
        <Fragment>
          <div className="container">
            <AlbomSale saleTitle={saleProducts[0].title} saleAuthor={saleProducts[0].author} saleContent={saleProducts[0].content} saleStar={<Star />} saleClosePrice={saleProducts[0].closePrice} saleOpenPrice={saleProducts[0].openPrice} saleBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <AlbomSale saleTitle={saleProducts[1].title} saleAuthor={saleProducts[1].author} saleContent={saleProducts[1].content} saleStar={<Star />} saleClosePrice={saleProducts[1].closePrice} saleOpenPrice={saleProducts[1].openPrice} saleBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <AlbomSale saleTitle={saleProducts[2].title} saleAuthor={saleProducts[2].author} saleContent={saleProducts[2].content} saleStar={<Star />} saleClosePrice={saleProducts[2].closePrice} saleOpenPrice={saleProducts[2].openPrice} saleBtnCart={<BtnAddToCart />} /><span className="mr">  </span>
            <AlbomSale saleTitle={saleProducts[3].title} saleAuthor={saleProducts[3].author} saleContent={saleProducts[3].content} saleStar={<Star />} saleClosePrice={saleProducts[3].closePrice} saleOpenPrice={saleProducts[3].openPrice} saleBtnCart={<BtnAddToCart />} />
          </div>
        </Fragment>
        <TitlePublisher titlePublisher={titleBlock[2].title} />
        <Fragment>
          <div className="container">
            <Publishers /><span className="size-under">  </span>
            <Publishers /><span className="size-under">  </span>
            <Publishers /><span className="size-under">  </span>
            <Publishers /><span className="size-under">  </span>
            <Publishers /><span className="size-under">  </span>
            <Publishers />

          </div>
        </Fragment>
        <Footer />
      </div>
    );
  }


}

export default App;



