import React, { Component } from 'react';
import "./styles/BtnAddToCart.scss";

export default class BtnAddToCart extends Component {
    render() {
        return (
            <button className="btn-add-to-cart">Add to cart</button>
        )
    }
}
