import React, { Component } from 'react';
import "./styles/AlbomSale.scss";
import sale from "./images/sale.svg"

export default class AlbomSale extends Component {
    render() {
        const { saleTitle, saleAuthor, saleStar, saleContent, saleClosePrice, saleOpenPrice, saleBtnCart } = this.props;

        return (
            <div className="albomSale">
                <img className="sale-icon" src={sale}></img>
                <img className="sale-image" src="https://picsum.photos/800/302/?random"></img>
                <p className="sale-title">{saleTitle} <span className="sale-author"><i>{saleAuthor}</i></span></p>
                <div className="sale-star">{saleStar}</div>
                <p className="sale-content">{saleContent}</p>
                <div className="sale-block-price">
                    <p className="sale-close-price"><s>${saleClosePrice}</s></p>
                    <p className="sale-open-price">${saleOpenPrice}</p>
                    {saleBtnCart}
                </div>
            </div>
        )
    }
}
