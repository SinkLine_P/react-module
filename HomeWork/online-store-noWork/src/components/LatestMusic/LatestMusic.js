import React, { Component } from 'react';
import "./styles/LatestMusic.scss";

export default class LatestMusic extends Component {
    render() {
        const { latestTitle, latestAuthor, latestContent, latestStar, latestPrice, latestBtnCart } = this.props;

        return (
            <div className="latestMusicCard">
                <img src="https://picsum.photos/800/301/?random"></img>
                <p className="latest-title">{latestTitle}  <span className="latest-author"><i>{latestAuthor}</i></span></p>
                <div className="latest-content">{latestContent}</div>
                <p className="latest-star">{latestStar}</p>
                <div className="latest-price-block">
                    <p className="latest-price">${latestPrice}</p>
                    {latestBtnCart}
                </div>
            </div>
        )
    }
}
