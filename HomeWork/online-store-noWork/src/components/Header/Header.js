import React, { Component } from 'react';
import "./styles/Header.scss";
import diskIcon from "./images/disk.png";

export default class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="header-logo">
                    <img href="#" src={diskIcon}></img>
                    <span className="header-title-M">M</span>
                    <span className="header-title">Store</span>
                </div>
                <div className="header-links">
                    <a href="#">HOME</a>
                    <a href="#">CD`s</a>
                    <a href="#">MP3</a>
                    <a href="#">MP4</a>
                    <a href="#">Video</a>
                </div>
            </div>
        )
    }
}
