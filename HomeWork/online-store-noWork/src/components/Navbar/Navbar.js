/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import "./styles/Navbar.scss";
import facebookIcon from "./images/facebook-icon.png";
import instagramIcon from "./images/instagram-icon.png";
import tiktokIcon from "./images/tiktok-icon.svg";
import cartIcon from "./images/cart-icon.png";

export default class Navbar extends Component {
    render() {
        return (
            <div className="navbar">
                <div className="navbar-icons">
                    <a href="#"><img src={facebookIcon}></img></a>
                    <a href="#"><img src={instagramIcon}></img></a>
                    <a href="#"><img src={tiktokIcon}></img></a>
                </div>
                <div className="navbar-cart">
                    <a href="#"><img src={cartIcon}></img></a>
                </div>
                <div className="navbar-login">
                    <a href="#">Login</a>
                    <span>/</span>
                    <a href="#">Register</a>
                </div>
            </div>
        )
    }
}
