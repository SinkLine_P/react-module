import React, { Component } from 'react';
import "./styles/Star.scss";
import starFill from "./images/star-fill.png";
import starUnFill from "./images/star-unfill.png";

export default class Star extends Component {
    render() {
        return (
            <div className="star">
                <img src={starFill}></img>
                <img src={starFill}></img>
                <img src={starFill}></img>
                <img src={starFill}></img>
                <img src={starUnFill}></img>
            </div>
        )
    }
}
