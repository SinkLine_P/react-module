import React from 'react';
import './modal-first.scss';
import './modal-second.scss';


class Modal extends React.Component {
    render() {
        const {
            // Modal First
            modalTitleHeaderFirst,
            ModalFirstContentPartOne,
            ModalFirstContentPartTwo,
            StylesModal,
            StylesModalHeader,
            StylesModalContent,
            StylesModalBtn,
            StylesModalSmooth,
            ModalbtnClose
        } = this.props;
        

        return (
            <div id="modal-window" className={StylesModalSmooth}>
                <div className={StylesModal}>
                    <div className={StylesModalHeader}>
                        <b>{modalTitleHeaderFirst}</b>
                        {ModalbtnClose}
                    </div>
                    <div className={StylesModalContent}>
                        <p>{ModalFirstContentPartOne}</p>
                        <p>{ModalFirstContentPartTwo}</p>
                    </div>
                    <div className={StylesModalBtn}>
                        <button>Ok</button>
                        <button>Cancel</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;