import React from 'react';
import "./Fragment.scss";

class Fragment extends React.Component {
    render() {
        const { PositionBtn, children } = this.props;

        return (
            <div className={PositionBtn}>
                {children}
            </div>
        )
    }
}

export default Fragment;