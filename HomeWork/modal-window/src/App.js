// import logo from './logo.svg';
import './App.css';
import React from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Fragment from './components/Fragment/Fragment';



class App extends React.Component {


  state = {
    // Modal First
    TitleFirstModal: "Open first modal",
    count: 0,
    modalTitleHeaderFirst: "Do you want to delete this file?",
    ModalFirstContentPartOne: "Once you delete this file, it won`t be possible to undo this action.",
    ModalFirstContentPartTwo: "Are you sure you want to delete it?",
    StylesModal: "modal-first",
    StylesModalHeader: "modal-first-header",
    StylesModalContent: "modal-first-content",
    StylesModalBtn: "modal-first-btn",
    StylesModalSmooth: "modal-smooth-first",
    // Modal Second
    counts: 0,
    TitleSecondModal: "Open second modal",
    modalTitleHeaderSecond: "Do you want to save this file?",
    ModalSecondContentPartOne: "Once you save this file, it won`t be possible to undo this action.",
    ModalSecondContentPartTwo: "Are you sure you want to save it?",
    StylesModalSecond: "modal-second",
    StylesModalHeaderSecond: "modal-second-header",
    StylesModalContentSecond: "modal-second-content",
    StylesModalBtnSecond: "modal-second-btn",
    StylesModalSmoothSecond: "modal-smooth-second",
    // Btn Modal
    PositionBtn: "positionBtnModal",
  }

  modalFirst = () => {
    this.setState(({ count }) => ({
      count: count + 1,
    }));
  }

  modalSecond = () => {
    this.setState(({ counts }) => ({
      counts: counts + 1,
    }));
  }

  modalClose = () => {
    this.setState(() => ({
      count: 0,
      counts: 0,
    }));
  }

  render() {
    const {
      // Modal First
      TitleFirstModal,
      modalTitleHeaderFirst,
      ModalFirstContentPartOne,
      ModalFirstContentPartTwo,
      StylesModal,
      StylesModalHeader,
      StylesModalContent,
      StylesModalBtn,
      StylesModalSmooth,
      // Modal Second
      TitleSecondModal,
      modalTitleHeaderSecond,
      ModalSecondContentPartOne,
      ModalSecondContentPartTwo,
      StylesModalSecond,
      StylesModalHeaderSecond,
      StylesModalContentSecond,
      StylesModalBtnSecond,
      StylesModalSmoothSecond,
      // Btn Modal
      PositionBtn,
    } = this.state


    return (
      <div className="App">

        <Fragment>
          {
            [...Array(this.state.count)].map(() =>
              <Modal
                StylesModalSmooth={StylesModalSmooth}
                modalTitleHeaderFirst={modalTitleHeaderFirst}
                ModalFirstContentPartOne={ModalFirstContentPartOne}
                ModalFirstContentPartTwo={ModalFirstContentPartTwo}
                StylesModal={StylesModal}
                StylesModalHeader={StylesModalHeader}
                StylesModalContent={StylesModalContent}
                StylesModalBtn={StylesModalBtn}
                ModalbtnClose={<button id="close-btn" onClick={this.modalClose}>X</button>}
              />)
          },
          {
            [...Array(this.state.counts)].map(() =>
              <Modal
                StylesModalSmooth={StylesModalSmoothSecond}
                modalTitleHeaderFirst={modalTitleHeaderSecond}
                ModalFirstContentPartOne={ModalSecondContentPartOne}
                ModalFirstContentPartTwo={ModalSecondContentPartTwo}
                StylesModal={StylesModalSecond}
                StylesModalHeader={StylesModalHeaderSecond}
                StylesModalContent={StylesModalContentSecond}
                StylesModalBtn={StylesModalBtnSecond}
                ModalbtnClose={<button id="close-btn" onClick={this.modalClose}>X</button>}
                />)
          }

        </Fragment>
        <Fragment PositionBtn={PositionBtn}>
          <Button onClick={this.modalFirst}>{TitleFirstModal}</Button>
          <Button onClick={this.modalSecond}>{TitleSecondModal}</Button>
        </Fragment>
      </div>
    );
  }


}

export default App;



