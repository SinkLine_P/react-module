import './App.scss';
import React from 'react';
import Header from './components/Header/Header';
import Body from './components/Body/Body';
import Footer from './components/Footer/Footer';


class App extends React.Component {

  
  state = {
    title: 'Mail client',
    user: {
      name: 'artem',
      age: 17,
    },
    footer: 'About',
    array: {
      arr: [{title: "titel-one", content: "text", data: "10.01.2021"},
            {title: "titel-two", content: "text-two", data: "13.12.2019"}],
    }
  }

  render() {
    const {title, user, footer, array} = this.state
    
    return (
      <div className="App">
        <Header title={title}/>
        <Body user={user} array={array}/>
        <Footer footer={footer}/>
      </div>
    )
  }
}

export default App;
