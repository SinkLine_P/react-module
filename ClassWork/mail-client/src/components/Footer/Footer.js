import React from 'react';


class Footer extends React.Component {
    render() {
        const {footer} = this.props;

        return (
            <div>
                <hr></hr>
                <p>{footer}</p>
            </div>
        )
    }
}

export default Footer;