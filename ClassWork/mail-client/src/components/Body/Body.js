import React from 'react';
import './body.scss';


class Body extends React.Component {
    render() {
        const { user } = this.props;
        const { array } = this.props;
        const arrayList = array.arr.map(title => title.map( content =>
            <div>content.title</div>
        ))
            
        return (
            <div>
                <p>{user.name}</p>
                <p>{user.age}</p>
                <div className="block-box">
                    {arrayList}
                </div>

            </div>
        )
    }
}

export default Body;