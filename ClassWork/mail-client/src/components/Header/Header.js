import React from 'react';


class Header extends React.Component {
    render() {
        const { title } = this.props;

        return (
            <h2>{title}</h2>
        )
    }
}

export default Header;