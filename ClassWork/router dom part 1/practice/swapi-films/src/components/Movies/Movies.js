import React, {Component} from 'react';
import Loading from "../Loading/Loading";
import axios from "axios";
import Movie from "../Movie/Movie";

class Movies extends Component {
  state = {
    movies: [],
    isLoading: true
  }

  async componentDidMount() {
    const movies = await axios('https://ajax.test-danit.com/api/swapi/films');
    this.setState({movies: movies.data, isLoading: false});
  }

  render() {
    const {isLoading, movies} = this.state;
    if (isLoading) return <Loading />;
    const moviesItems = movies
      .map(movie => <Movie key={movie.id} movie={movie} />)

    return (
      <ol>
        {moviesItems}
      </ol>
    );
  }
}

export default Movies;