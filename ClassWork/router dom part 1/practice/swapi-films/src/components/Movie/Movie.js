import React, {Component} from 'react';
import Characters from "../Characters/Characters";

class Movie extends Component {
  state = {
    isExpanded: false
  }

  expandHandler = () => {
    this.setState({isExpanded: true});
  }

  render() {
    const {isExpanded} = this.state;
    const {movie} = this.props;

    return (
      <li>
        <h3>{movie.name}</h3>
        {!isExpanded && <button onClick={this.expandHandler}>Show more</button>}
        {isExpanded && <div>
          <p>{movie.episodeId}</p>
          <p>{movie.openingCrawl}</p>
          <Characters chars={movie.characters}/>
        </div>}
      </li>
    );
  }
}

export default Movie;