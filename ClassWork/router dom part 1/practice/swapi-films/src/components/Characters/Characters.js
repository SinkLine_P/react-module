import React, {Component} from 'react';
import axios from "axios";

class Characters extends Component {
  state = {
    chars: [],
    isLoading: true
  }

  async componentDidMount() {
    const charLinks = this.props.chars;
    const chars = await Promise.all(charLinks.map(link => axios(link)))
    const charsData = chars.map(c => c.data);
    this.setState({chars: charsData, isLoading: false})
  }

  render() {
    return (
      <p>
        {this.state.chars.map(char => char.name).join(', ')}
      </p>
    );
  }
}

export default Characters;