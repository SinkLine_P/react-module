import React, {Component} from 'react';
import Button from "../Button/Button";

class Number extends Component {
  render() {
    const {number, deleteNumber, index} = this.props;

    return (
      <div id={number}>
        <span>{number}</span>
        <Button onClick={() => deleteNumber(index)}>X</Button>
      </div>
    );
  }
}

export default Number;