import React, {Component} from 'react';
import Number from "../Number/Number";

class Numbers extends Component {
  render() {
    const {numbers, deleteNumber} = this.props;
    const numberItems = numbers.map((number, index) => <Number key={index} index={index} deleteNumber={deleteNumber} number={number}/>);

    return (
      <div>
        {numberItems}
      </div>
    );
  }
}

export default Numbers;