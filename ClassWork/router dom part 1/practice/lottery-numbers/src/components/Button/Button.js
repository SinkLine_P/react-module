import React, {Component} from 'react';

class Button extends Component {
  render() {
    const {children, onClick, disabled} = this.props;
    return (
      <button disabled={disabled} onClick={onClick}>
        {children}
      </button>
    );
  }
}

export default Button;