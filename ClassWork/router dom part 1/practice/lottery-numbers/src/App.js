import './App.css';
import Button from "./components/Button/Button";
import React from 'react';
import Numbers from "./components/Numbers/Numbers";

class App extends React.Component {
  state = {
    numbers: [],
  }

  generateNumber = () => {
    const {numbers} = this.state;
    if (numbers.length >= 6) return;
    let num;

    do {
      num = Math.ceil(Math.random()*6);
    } while (numbers.includes(num))

    this.setState({numbers: [...this.state.numbers, num]})
  }

  deleteNumber = (index) => {
    this.setState({
      numbers: this.state.numbers.filter((item, indx) => indx !== index)
    })
  }

  render() {
    const {numbers} = this.state;

    return (
      <div className="App">
        <Button disabled={numbers.length >= 6} onClick={this.generateNumber}>Generate</Button>
        <Numbers deleteNumber={this.deleteNumber} numbers={numbers} />
      </div>
    )
  };
}

export default App;
