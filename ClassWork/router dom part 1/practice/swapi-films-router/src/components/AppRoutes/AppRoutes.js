import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Movies from "../Movies/Movies";
import FilmInfo from "../FilmInfo/FilmInfo";

const AppRoutes = () => (
  <Switch>
    <Redirect exact from="/" to="/films"/>
    <Route exact path="/films">
      <Movies />
    </Route>
    <Route exact path="/films/:filmId">
      <FilmInfo />
    </Route>
  </Switch>
);

export default AppRoutes;