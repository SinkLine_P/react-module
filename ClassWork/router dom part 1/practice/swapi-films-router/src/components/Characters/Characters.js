import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Loading from '../Loading/Loading'

const Characters = ({ chars }) => {
  const [characters, setCharacters] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const requests = chars.map(link => axios(link))
    Promise.all(requests)
      .then(res => {
        setCharacters(res.map(r => r.data))
        setIsLoading(false)
      })
  }, [chars])

  if (isLoading) {
    return <Loading />
  }

  return (
    <p>
      {characters.map(char => char.name).join(', ')}
    </p>
  )
}

export default Characters
