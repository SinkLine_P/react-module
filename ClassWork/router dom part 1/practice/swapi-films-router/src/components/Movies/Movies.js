import React, { useEffect, useState } from 'react'
import Loading from '../Loading/Loading'
import axios from 'axios'
import Movie from '../Movie/Movie'

const Movies = () => {
  const [movies, setMovies] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    axios('https://ajax.test-danit.com/api/swapi/films')
      .then(res => {
        setMovies(res.data)
        setIsLoading(false)
      })
  }, [])

  if (isLoading) return <Loading />
  const moviesItems = movies
    .map(movie => <Movie key={movie.id} movie={movie} />)

  return (
    <ol>
      {moviesItems}
    </ol>
  )
}

export default Movies
