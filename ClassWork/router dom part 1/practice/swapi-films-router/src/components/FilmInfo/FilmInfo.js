import React, {useState, useEffect} from 'react';
import Characters from "../Characters/Characters";
import {useParams, Link} from 'react-router-dom'
import axios from "axios";
import Loading from "../Loading/Loading";

const FilmInfo = () => {
  const params = useParams()
  const [movie, setMovie] = useState(null)
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    setIsLoading(true)
    axios(`https://ajax.test-danit.com/api/swapi/films/${params.filmId}`)
      .then(res => {
        setMovie(res.data)
        setIsLoading(false)
      })
  }, [params.filmId])

  if(isLoading) {
    return <Loading />
  }

  return (
    <div>
      <h3>{movie.name}</h3>
      <div>
        <p>{movie.episodeId}</p>
        <p>{movie.openingCrawl}</p>
        <Characters chars={movie.characters}/>
      </div>
      <Link to="/films">Back</Link>
    </div>
  )
};

export default FilmInfo;