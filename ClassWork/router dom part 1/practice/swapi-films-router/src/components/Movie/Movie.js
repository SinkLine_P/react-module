import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import Characters from '../Characters/Characters'

const Movie = ({movie}) => {

  const history = useHistory()

  const expandFilm = () => {
    history.push(`/films/${movie.id}`)
  }

  return (
    <li>
      <h3>{movie.name}</h3>
      <button onClick={expandFilm}>Show more</button>
    </li>
  )
}

export default Movie
