import React from 'react'
import Movies from './components/Movies/Movies'
import AppRoutes from "./components/AppRoutes/AppRoutes";

const App = () => {
  return (
    <AppRoutes />
  )
}

export default App
