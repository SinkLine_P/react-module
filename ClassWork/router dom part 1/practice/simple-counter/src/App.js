import React from 'react';
import './App.css';

class App extends React.Component {
  state = {
    num: 50,
    delta: 5
  }

  render() {
    const {num, delta} = this.state;
    return (
      <div className="App">
        <div>
          <button onClick={this.decrease}>-</button>
          <span>{num}%</span>
          <button onClick={this.increase}>+</button>
        </div>
        <div>
          <button onClick={this.decreaseDelta}>-</button>
          <span>{delta}</span>
          <button onClick={this.increaseDelta}>+</button>
        </div>
      </div>
    );
  }

  increase = () => {
    const {num, delta} = this.state;

    if (num + delta <= 100) {
      this.setState({num: num + delta})
    } else {
      this.setState({num: 100})
    }
  }

  decrease = () => {
    const {num, delta} = this.state;
    if (num - delta >= 0) {
      this.setState({num: num - delta})
    } else {
      this.setState({num: 0})
    }
  }

  increaseDelta = () => {
    const {delta} = this.state;
    if (delta < 10) {
      this.setState({delta: delta + 1})
    }
  }

  decreaseDelta = () => {
    const {delta} = this.state;
    if (delta > 0) {
      this.setState({delta: delta - 1})
    }
  }
}

export default App;
