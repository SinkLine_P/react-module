import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";
import Inbox from "../pages/Inbox/Inbox";
import Favorites from "../pages/Favorites/Favorites";
import Sent from "../pages/Sent/Sent";
import Page404 from "../pages/Page404/Page404";
import OneEmail from "../pages/OneEmail/OneEmail";
import Login from "../pages/Login/Login";

const AppRoutes = ({emails}) => {
  console.log('Rendering AppRoutes')
  return (
    <Switch>
      <Redirect exact from='/' to='/inbox'/>
      <Route exact path='/inbox'>
        <Inbox emails={emails}/>
      </Route>
      <Route exact path='/inbox/:emailId'><OneEmail /></Route>
      {/*<Route path='/inbox' render={(...routerProps) => <Inbox emails={emails} {...routerProps} />}/>*/}
      <Route exact path='/favorites'><Favorites/></Route>
      <Route exact path='/login'><Login/></Route>
      <Route exact path='/sent'><Sent/></Route>
      <Route path='*'><Page404/></Route>
    </Switch>
  )
};

export default AppRoutes;