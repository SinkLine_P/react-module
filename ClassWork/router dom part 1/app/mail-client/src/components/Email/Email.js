import React from 'react';
import './Email.scss';
import PropTypes from 'prop-types';
import Icon from "../Icon/Icon";
import {Link} from "react-router-dom";

// function Email() {}
const Email = ({email, showFull}) => {
  return (
    <div className='email'>
      <Link to={`/inbox/${email.id}`}>{email.title}</Link>
      <Icon type='star'/>
      {showFull && <p>{email.text}</p>}
    </div>
  )
}

// string, bool, func, object, array, number, symbol
// instanceOf(), oneOf([]), oneOfType([]), arrayOf(), shape(), exact()
Email.propTypes = {
  // email: PropTypes.object.isRequired,
  // decision: PropTypes.oneOf(['yes', 'no'])
  email: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string
  }).isRequired
}

export default Email;