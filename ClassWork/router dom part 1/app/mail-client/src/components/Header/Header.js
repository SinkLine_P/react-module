import React from 'react';
import PropTypes from 'prop-types';

class Header extends React.Component {
  render() {
    const {title} = this.props;
    // console.log(this, 'Header.js')

    return (
      <div>
        <h2>{title}</h2>
      </div>
    )
  }

  // static defaultProps = {}
}

Header.propTypes = {
  title: PropTypes.string
}

Header.defaultProps = {
  title: 'Mail client'
}

export default Header;