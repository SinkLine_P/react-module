import React from 'react';
import * as Icons from '../../theme/icons'
import './Icon.scss'

const Icon = ({type, color, filled}) => {
  const jsx = Icons[type];

  if (!jsx) {
    return null;
  }

  return (
    <div className={`icon icon--${type}`}>
      {jsx({color, filled})}
    </div>
  )
};

export default Icon;