import React from 'react';
import {NavLink} from "react-router-dom";
import './Sidebar.scss';

const Sidebar = () => (
  <nav>
    <ul>
      <li><NavLink exact to='/inbox' activeClassName='link__selected'>Inbox</NavLink></li>
      <li><NavLink exact to='/sent' activeClassName='link__selected'>Sent</NavLink></li>
      <li><NavLink exact to='/login' activeClassName='link__selected'>Login</NavLink></li>
      <li><NavLink exact to='/favorites' activeClassName='link__selected'>Favorite</NavLink></li>
      <li><NavLink exact to='/notexits' activeClassName='link__selected'>Path does not exist</NavLink></li>
    </ul>
  </nav>
);

export default Sidebar;