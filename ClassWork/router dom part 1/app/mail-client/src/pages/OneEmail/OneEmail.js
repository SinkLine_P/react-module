import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom'
import Email from "../../components/Email/Email";
import axios from "axios";
import Loader from "../../components/Loader/Loader";

const OneEmail = () => {
  const [email, setEmail] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const {emailId} = useParams()
  const history = useHistory()

  useEffect(() => {
    setLoading(true);
    axios(`/api/emails/${emailId}`)
      .then(res => {
        setEmail(res.data);
        setLoading(false);
      })
  }, [emailId])

  if (isLoading) {
    return <Loader/>
  }

  const goToPrevious = () => {
    history.push(`/inbox/${emailId - 1}`)
  }

  const goToNext = () => {
    history.push(`/inbox/${+emailId + 1}`)
  }

  return (
    <div>
      <Email email={email} showFull/>
      <button onClick={goToPrevious}>Previous</button>
      <button onClick={goToNext}>Next</button>
    </div>
  )
};


export default OneEmail;