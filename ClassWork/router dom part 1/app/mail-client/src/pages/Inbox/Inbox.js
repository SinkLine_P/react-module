import React from 'react';
import Email from "../../components/Email/Email";
import PropTypes from "prop-types";

const Inbox = ({emails}) => {
  const emailCards = emails
    .map(email => <Email key={email.id} email={email}/>)

  return (
    <div>
      {emails.length === 0 && <h4>You don't have any emails yet</h4>}
      {emailCards}
    </div>
  )
}

Inbox.propTypes = {
  // emails: PropTypes.array.isRequired
  emails: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string
  })).isRequired
}

export default Inbox;