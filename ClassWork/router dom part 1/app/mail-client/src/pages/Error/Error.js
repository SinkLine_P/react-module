import React, {Component} from 'react';

class Error extends Component {
  render() {
    return (
      <div>
        <h2>An error has occurred</h2>
        <h4>Try refreshing the page, and if the problem persists, contact our team at aaa@a.com</h4>
      </div>
    );
  }
}

export default Error;