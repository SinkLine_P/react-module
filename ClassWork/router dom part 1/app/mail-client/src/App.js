import React, {useEffect, useState} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Inbox from "./pages/Inbox/Inbox";
import Loader from "./components/Loader/Loader";
import axios from 'axios';
import AppRoutes from "./routes/AppRoutes";
import Sidebar from "./components/Sidebar/Sidebar";

const App = () => {
  const [emails, setEmails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios('/api/emails')
      .then(res => {
        setEmails(res.data);
        setIsLoading(false);
      })
  }, []);

  if (isLoading) {
    return <Loader/>
  }

  return (
    <div className="App">
      <Header/>
      <Sidebar/>
      <AppRoutes emails={emails}/>
      <Footer/>
    </div>
  )
}

export default App;
