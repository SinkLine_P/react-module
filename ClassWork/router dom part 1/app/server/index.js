const express = require('express');
// import express from 'express'

const app = express();

const port = 8085;

const emails = [
  {"id": 1, "title": "Title 1", "text": "Text 1 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aspernatur deserunt distinctio doloremque eos iste maiores sequi sunt ullam ut! Aliquam, amet cum esse et mollitia nam quas recusandae veniam!", "favorite":  true},
  {"id": 2, "title": "Title 2", "text": "Text 2 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aspernatur deserunt distinctio doloremque eos iste maiores sequi sunt ullam ut! Aliquam, amet cum esse et mollitia nam quas recusandae veniam!"},
  {"id": 3, "title": "Title 3", "text": "Text 3 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aspernatur deserunt distinctio doloremque eos iste maiores sequi sunt ullam ut! Aliquam, amet cum esse et mollitia nam quas recusandae veniam!"}
]

app.get('/api/emails', (req, res) => {
  res.send(emails)
})

app.get('/api/emails/:emailId', (req, res) => {
  const emailId = +req.params.emailId
  const email = emails.find(e => e.id === emailId)
  res.send(email)
})

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})